# Servicio - Estructura de Datos

Este documento describe el **Servicio de Estructura de Datos**, que proporciona una API para varios algoritmos y funcionalidades relacionadas con la manipulación de datos. El servicio se divide en diferentes talleres, cada uno de los cuales se enfoca en una tarea y/o requerimiento específico.



# REQUERIMIENTOS

> **Nota:** todas las entregas se haran sobre el mismo repositorio, no olvide hacer un buen manejo de versiones, genere un tag por cada algoritmo que suba.

...

# Requerimiento 1 (Taller 5) - API de Algoritmos

**a).** Desarrollo de un Servicio de API

En este taller, se ha desarrollado un servicio que proporciona un conjunto de recursos para diferentes algoritmos. Cada algoritmo puede ser solicitado a través de la API del servicio.

**b).** Implementación de la Búsqueda de Palabras

Se ha agregado un recurso especializado en el servicio, denominado `/indices-invertidos`, que permite realizar búsquedas de palabras. Este recurso se invoca utilizando el verbo POST.


```http
POST /indices-invertidos
```


### Video de Apoyo

Para obtener más información sobre el funcionamiento del servicio y la implementación de la búsqueda de palabras usando indices invertidos, pueden consultar el siguiente video:

> [Ver Video de Apoyo](https://youtu.be/dqoUlnzRWT4) 


# Requerimiento 2 (Taller 6) - [Nombre del Taller]

[Descripción del contenido y actividades del Taller 6]

# Requerimiento 3 (Taller 7) - [Nombre del Taller]

[Descripción del contenido y actividades del Taller 7]

# Requerimiento 4 (Taller 8) - [Nombre del Taller]

[Descripción del contenido y actividades del Taller 8]

# Requerimiento 5 (Taller 9) - [Nombre del Taller]

[Descripción del contenido y actividades del Taller 9]

# Requerimiento 6 (Taller 10) - [Nombre del Taller]

[Descripción del contenido y actividades del Taller 10]


